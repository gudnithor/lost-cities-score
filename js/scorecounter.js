angular.module('app', ['ngTouch'])
	.controller('ScoreCounterController', function ($scope) {

		$scope.cardgroups = [
			{color:'Red', cards: [
				{name:'M', checked:false}, {name:'M', checked:false}, {name:'M', checked:false},
				{name:'2', checked:false}, {name:'3', checked:false}, {name:'4', checked:false},
				{name:'5', checked:false}, {name:'6', checked:false}, {name:'7', checked:false}, 
				{name:'8', checked:false}, {name:'9', checked:false}, {name:'10', checked:false}
			]},
			{color:'Green', cards: [
				{name:'M', checked:false}, {name:'M', checked:false}, {name:'M', checked:false},
				{name:'2', checked:false}, {name:'3', checked:false}, {name:'4', checked:false},
				{name:'5', checked:false}, {name:'6', checked:false}, {name:'7', checked:false}, 
				{name:'8', checked:false}, {name:'9', checked:false}, {name:'10', checked:false}
			]},
			{color:'White', cards: [
				{name:'M', checked:false}, {name:'M', checked:false}, {name:'M', checked:false},
				{name:'2', checked:false}, {name:'3', checked:false}, {name:'4', checked:false},
				{name:'5', checked:false}, {name:'6', checked:false}, {name:'7', checked:false}, 
				{name:'8', checked:false}, {name:'9', checked:false}, {name:'10', checked:false}
			]},
			{color:'Blue', cards: [
				{name:'M', checked:false}, {name:'M', checked:false}, {name:'M', checked:false},
				{name:'2', checked:false}, {name:'3', checked:false}, {name:'4', checked:false},
				{name:'5', checked:false}, {name:'6', checked:false}, {name:'7', checked:false}, 
				{name:'8', checked:false}, {name:'9', checked:false}, {name:'10', checked:false}
			]},
			{color:'Yellow', cards: [
				{name:'M', checked:false}, {name:'M', checked:false}, {name:'M', checked:false},
				{name:'2', checked:false}, {name:'3', checked:false}, {name:'4', checked:false},
				{name:'5', checked:false}, {name:'6', checked:false}, {name:'7', checked:false}, 
				{name:'8', checked:false}, {name:'9', checked:false}, {name:'10', checked:false}
			]},
		];

		$scope.scoreTable = [
			{color: 'Red', style: 'list-group-item-danger', score: 0},
			{color: 'Green', style: 'list-group-item-success', score: 0},
			{color: 'White', style: '', score: 0},
			{color: 'Blue', style: 'list-group-item-info', score: 0},
			{color: 'Yellow', style: 'list-group-item-warning', score: 0}
		];

		$scope.totalScore = 0;

		// Resets a certain cardgroup
		$scope.resetCardgroup = function(color) {

			angular.forEach($scope.cardgroups, function(value) {
				if (value.color == color)
				{
					angular.forEach(value.cards, function(value) {
						value.checked = false;
					});

					$scope.setScore(color, 0);
				}
			});
		}

		// Calculates the new score (called by ng-click)
		$scope.calculateScore = function(color) {

			$scope.totalScore = 0;

			// Look into using a filter here.
			angular.forEach($scope.cardgroups, function(value) {
				if (value.color == color)
				{
					var score = 0;
					var multiplierValue = 1;
					var isNoCardChecked = true;

					angular.forEach(value.cards, function(value) {
						if (value.checked)
						{
							isNoCardChecked = false;

							if (value.name == 'M')
								multiplierValue++;
							else
								score += parseInt(value.name);
						}
					});

					if (!isNoCardChecked)
						score = (score - 20) * multiplierValue;
					else
						score = 0;

					$scope.setScore(color, score);
				}
			});
		};

		// Updates the specified color score in the scoreTable and calculates the new totalScore
		$scope.setScore = function(color, score) {
			$scope.totalScore = 0;

			angular.forEach($scope.scoreTable, function(value) {
				if (value.color == color)
					value.score = score;

				$scope.totalScore += value.score;

			});
		};
	});

