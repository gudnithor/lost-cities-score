#Lost Cities Score Counter

Simple score counter for the board game Lost Cities built using angular.js and bootstrap. Made to try out angular and I also needed a better way to calculate the scoring.

[gudnithor.github.io/lost-cities-score] (https://gudnithor.github.io/lost-cities-score/)

## Tips and tricks

You can swipe to the left over each group of cards to clear those cards. Courtesy of ngTouch.
